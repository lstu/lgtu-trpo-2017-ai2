/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2008                    */
/* Created on:     13.05.2017 11:08:16                          */
/*==============================================================*/


if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Category') and o.name = 'FK_CATEGORY_CONSIST_SECTION')
alter table Category
   drop constraint FK_CATEGORY_CONSIST_SECTION
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Club') and o.name = 'FK_CLUB_DIRECT2_MANAGER')
alter table Club
   drop constraint FK_CLUB_DIRECT2_MANAGER
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Participation') and o.name = 'FK_PARTICIP_ISIN_CATEGORY')
alter table Participation
   drop constraint FK_PARTICIP_ISIN_CATEGORY
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Participation') and o.name = 'FK_PARTICIP_TAKEPART_SPORTSMA')
alter table Participation
   drop constraint FK_PARTICIP_TAKEPART_SPORTSMA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Participation') and o.name = 'FK_PARTICIP_TAKEPART__SPORTSMA')
alter table Participation
   drop constraint FK_PARTICIP_TAKEPART__SPORTSMA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Referee') and o.name = 'FK_REFEREE_REFEREE_MANAGER')
alter table Referee
   drop constraint FK_REFEREE_REFEREE_MANAGER
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Referee') and o.name = 'FK_REFEREE_REFEREE2_CATEGORY')
alter table Referee
   drop constraint FK_REFEREE_REFEREE2_CATEGORY
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Section') and o.name = 'FK_SECTION_INCLUDE_TORNAMEN')
alter table Section
   drop constraint FK_SECTION_INCLUDE_TORNAMEN
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Sportsman') and o.name = 'FK_SPORTSMA_WORK_CLUB')
alter table Sportsman
   drop constraint FK_SPORTSMA_WORK_CLUB
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Tornament') and o.name = 'FK_TORNAMEN_REALIZE_CLUB')
alter table Tornament
   drop constraint FK_TORNAMEN_REALIZE_CLUB
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Training') and o.name = 'FK_TRAINING_TRAINING_MANAGER')
alter table Training
   drop constraint FK_TRAINING_TRAINING_MANAGER
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Training') and o.name = 'FK_TRAINING_TRAINING2_SPORTSMA')
alter table Training
   drop constraint FK_TRAINING_TRAINING2_SPORTSMA
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Category')
            and   name  = 'Consist_FK'
            and   indid > 0
            and   indid < 255)
   drop index Category.Consist_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Category')
            and   type = 'U')
   drop table Category
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Club')
            and   name  = 'Direct2_FK'
            and   indid > 0
            and   indid < 255)
   drop index Club.Direct2_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Club')
            and   type = 'U')
   drop table Club
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Manager')
            and   type = 'U')
   drop table Manager
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Participation')
            and   name  = 'TakePart_Partner_FK'
            and   indid > 0
            and   indid < 255)
   drop index Participation.TakePart_Partner_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Participation')
            and   name  = 'TakePart_FK'
            and   indid > 0
            and   indid < 255)
   drop index Participation.TakePart_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Participation')
            and   type = 'U')
   drop table Participation
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Referee')
            and   name  = 'Referee_FK'
            and   indid > 0
            and   indid < 255)
   drop index Referee.Referee_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Referee')
            and   name  = 'Referee2_FK'
            and   indid > 0
            and   indid < 255)
   drop index Referee.Referee2_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Referee')
            and   type = 'U')
   drop table Referee
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Section')
            and   name  = 'Include_FK'
            and   indid > 0
            and   indid < 255)
   drop index Section.Include_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Section')
            and   type = 'U')
   drop table Section
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Sportsman')
            and   name  = 'Work_FK'
            and   indid > 0
            and   indid < 255)
   drop index Sportsman.Work_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Sportsman')
            and   type = 'U')
   drop table Sportsman
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Tornament')
            and   name  = 'Realize_FK'
            and   indid > 0
            and   indid < 255)
   drop index Tornament.Realize_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Tornament')
            and   type = 'U')
   drop table Tornament
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Training')
            and   name  = 'Training_FK'
            and   indid > 0
            and   indid < 255)
   drop index Training.Training_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Training')
            and   name  = 'Training2_FK'
            and   indid > 0
            and   indid < 255)
   drop index Training.Training2_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Training')
            and   type = 'U')
   drop table Training
go

/*==============================================================*/
/* Table: Category                                              */
/*==============================================================*/
create table Category (
   CategoryID           int                  not null,
   TournamentName       varchar(50)          not null,
   TournamentDate       datetime             not null,
   SectionID            int                  not null,
   CategoryClassStart   char(1)              not null,
   CategoryClassMax     char(1)              not null,
   CategoryDateMin      datetime             not null,
   CategoryDateMax      datetime             not null,
   CategoryName         varchar(100)         not null,
   constraint PK_CATEGORY primary key nonclustered (CategoryID)
)
go

/*==============================================================*/
/* Index: Consist_FK                                            */
/*==============================================================*/
create index Consist_FK on Category (
TournamentName ASC,
TournamentDate ASC,
SectionID ASC
)
go

/*==============================================================*/
/* Table: Club                                                  */
/*==============================================================*/
create table Club (
   ClubCity             varchar(30)          not null,
   ClubName             varchar(100)         not null,
   ManagerID            int                  not null,
   constraint PK_CLUB primary key nonclustered (ClubCity, ClubName)
)
go

/*==============================================================*/
/* Index: Direct2_FK                                            */
/*==============================================================*/
create index Direct2_FK on Club (
ManagerID ASC
)
go

/*==============================================================*/
/* Table: Manager                                               */
/*==============================================================*/
create table Manager (
   ManagerID            int                  not null,
   ManagerName          varchar(50)          not null,
   ManagerSurname       varchar(50)          null,
   ManagerPatronymic    varchar(50)          null,
   ManagerCategory      varchar(3)           null,
   ManagerPassword      varchar(25)          null,
   constraint PK_MANAGER primary key nonclustered (ManagerID)
)
go

/*==============================================================*/
/* Table: Participation                                         */
/*==============================================================*/
create table Participation (
   CategoryID           int                  not null,
   ParticipationNumber  int                  not null,
   SportsmanID          int                  not null,
   Spo_SportsmanID      int                  null,
   ParticipationResult  int                  null,
   constraint PK_PARTICIPATION primary key nonclustered (CategoryID, ParticipationNumber)
)
go

/*==============================================================*/
/* Index: TakePart_FK                                           */
/*==============================================================*/
create index TakePart_FK on Participation (
SportsmanID ASC
)
go

/*==============================================================*/
/* Index: TakePart_Partner_FK                                   */
/*==============================================================*/
create index TakePart_Partner_FK on Participation (
Spo_SportsmanID ASC
)
go

/*==============================================================*/
/* Table: Referee                                               */
/*==============================================================*/
create table Referee (
   ManagerID            int                  not null,
   CategoryID           int                  not null,
   constraint PK_REFEREE primary key nonclustered (ManagerID, CategoryID)
)
go

/*==============================================================*/
/* Index: Referee2_FK                                           */
/*==============================================================*/
create index Referee2_FK on Referee (
CategoryID ASC
)
go

/*==============================================================*/
/* Index: Referee_FK                                            */
/*==============================================================*/
create index Referee_FK on Referee (
ManagerID ASC
)
go

/*==============================================================*/
/* Table: Section                                               */
/*==============================================================*/
create table Section (
   TournamentName       varchar(50)          not null,
   TournamentDate       datetime             not null,
   SectionID            int                  not null,
   SectionName          varchar(100)         not null,
   SectionStartTime     datetime             not null,
   constraint PK_SECTION primary key nonclustered (TournamentName, TournamentDate, SectionID)
)
go

/*==============================================================*/
/* Index: Include_FK                                            */
/*==============================================================*/
create index Include_FK on Section (
TournamentName ASC,
TournamentDate ASC
)
go

/*==============================================================*/
/* Table: Sportsman                                             */
/*==============================================================*/
create table Sportsman (
   SportsmanID          int                  not null,
   ClubCity             varchar(30)          not null,
   ClubName             varchar(100)         not null,
   SportsmanName        varchar(50)          not null,
   SportsmanSurname     varchar(50)          not null,
   SportsmanPatronymic  varchar(50)          null,
   SportsmanSex         char(1)              not null,
   SportsmanDate        datetime             not null,
   SportsmanClass       char(1)              not null,
   SportsmanPartnerID   int                  null,
   SportsmanPassword    varchar(25)          null,
   constraint PK_SPORTSMAN primary key nonclustered (SportsmanID)
)
go

/*==============================================================*/
/* Index: Work_FK                                               */
/*==============================================================*/
create index Work_FK on Sportsman (
ClubCity ASC,
ClubName ASC
)
go

/*==============================================================*/
/* Table: Tornament                                             */
/*==============================================================*/
create table Tornament (
   TournamentName       varchar(50)          not null,
   TournamentDate       datetime             not null,
   ClubCity             varchar(30)          not null,
   ClubName             varchar(100)         not null,
   TournamentStartTime  datetime             not null,
   TournamentDescription varchar(1000)        null,
   constraint PK_TORNAMENT primary key nonclustered (TournamentName, TournamentDate)
)
go

/*==============================================================*/
/* Index: Realize_FK                                            */
/*==============================================================*/
create index Realize_FK on Tornament (
ClubCity ASC,
ClubName ASC
)
go

/*==============================================================*/
/* Table: Training                                              */
/*==============================================================*/
create table Training (
   ManagerID            int                  not null,
   SportsmanID          int                  not null,
   constraint PK_TRAINING primary key nonclustered (ManagerID, SportsmanID)
)
go

/*==============================================================*/
/* Index: Training2_FK                                          */
/*==============================================================*/
create index Training2_FK on Training (
SportsmanID ASC
)
go

/*==============================================================*/
/* Index: Training_FK                                           */
/*==============================================================*/
create index Training_FK on Training (
ManagerID ASC
)
go

alter table Category
   add constraint FK_CATEGORY_CONSIST_SECTION foreign key (TournamentName, TournamentDate, SectionID)
      references Section (TournamentName, TournamentDate, SectionID)
go

alter table Club
   add constraint FK_CLUB_DIRECT2_MANAGER foreign key (ManagerID)
      references Manager (ManagerID)
go

alter table Participation
   add constraint FK_PARTICIP_ISIN_CATEGORY foreign key (CategoryID)
      references Category (CategoryID)
go

alter table Participation
   add constraint FK_PARTICIP_TAKEPART_SPORTSMA foreign key (SportsmanID)
      references Sportsman (SportsmanID)
go

alter table Participation
   add constraint FK_PARTICIP_TAKEPART__SPORTSMA foreign key (Spo_SportsmanID)
      references Sportsman (SportsmanID)
go

alter table Referee
   add constraint FK_REFEREE_REFEREE_MANAGER foreign key (ManagerID)
      references Manager (ManagerID)
go

alter table Referee
   add constraint FK_REFEREE_REFEREE2_CATEGORY foreign key (CategoryID)
      references Category (CategoryID)
go

alter table Section
   add constraint FK_SECTION_INCLUDE_TORNAMEN foreign key (TournamentName, TournamentDate)
      references Tornament (TournamentName, TournamentDate)
go

alter table Sportsman
   add constraint FK_SPORTSMA_WORK_CLUB foreign key (ClubCity, ClubName)
      references Club (ClubCity, ClubName)
go

alter table Tornament
   add constraint FK_TORNAMEN_REALIZE_CLUB foreign key (ClubCity, ClubName)
      references Club (ClubCity, ClubName)
go

alter table Training
   add constraint FK_TRAINING_TRAINING_MANAGER foreign key (ManagerID)
      references Manager (ManagerID)
go

alter table Training
   add constraint FK_TRAINING_TRAINING2_SPORTSMA foreign key (SportsmanID)
      references Sportsman (SportsmanID)
go

